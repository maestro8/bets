package com.sample.bets

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.fragment.dialog
import androidx.navigation.fragment.fragment
import com.sample.bets.NavGraph.Destination.predictions
import com.sample.bets.view.predictions.EnterPredictionView
import com.sample.bets.view.predictions.MatchKey
import com.sample.bets.view.predictions.PredictionsView
import com.sample.bets.view.results.ResultView
import dagger.Binds
import dagger.Module
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import javax.inject.Inject
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class NavigationModule {
    @Binds
    @IntoSet
    abstract fun bindNavigationGraphDestinationProvider(fragment: PredictionsViewDestination): NavigationGraphDestinationProvider
}

interface NavigationGraphDestinationProvider {
    operator fun invoke(graphBuilder: NavGraphBuilder)
}

@Singleton
class PredictionsViewDestination @Inject constructor() : NavigationGraphDestinationProvider {
    override fun invoke(graphBuilder: NavGraphBuilder) {
        graphBuilder.apply {
            fragment<PredictionsView>(predictions) {
                label = "Predictions"
            }
            fragment<ResultView>(NavGraph.Destination.results){
                label = "Results"
                action(NavGraph.Action.resetPredictions) {
                    navOptions {
                        launchSingleTop = true
                        popUpTo = predictions
                    }
                }
            }
            dialog<EnterPredictionView>(NavGraph.Destination.enterPrediction){
                label = "EnterPrediction"
                argument(MatchKey::class.java.simpleName) {
                    type = NavType.ParcelableType(MatchKey::class.java)
                }
            }
        }
    }
}

@EntryPoint
@InstallIn(ActivityComponent::class)
interface DestinationProvidersEntryPoints {
    operator fun invoke(): Set<NavigationGraphDestinationProvider>
}

object NavGraph {
    const val id = 1
    object Destination {
        const val predictions = 2
        const val enterPrediction = 3
        const val results = 4
    }
    object Action {
        const val resetPredictions = 5
    }
}