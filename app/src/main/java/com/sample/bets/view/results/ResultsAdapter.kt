package com.sample.bets.view.results

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sample.bets.R
import com.sample.bets.business.results.MatchResultWithPrediction

class ResultsAdapter : RecyclerView.Adapter<ResultsAdapter.ResultsViewHolder>() {
    private val resultsInternal: MutableList<MatchResultWithPrediction> = mutableListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.result_item, parent, false)
        return ResultsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ResultsViewHolder, position: Int) {
        val matchesWithPredictions = resultsInternal[position]
        holder.setMatch(matchesWithPredictions)
    }

    override fun getItemCount(): Int = resultsInternal.size

    fun updateData(results: List<MatchResultWithPrediction>) = resultsInternal.apply {
        clear()
        addAll(results)
        notifyDataSetChanged()
    }

    class ResultsViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        private val team1NameView = itemView.findViewById<TextView>(R.id.resultsViewTeam1Name)
        private val team2NameView = itemView.findViewById<TextView>(R.id.resultsViewTeam2Name)
        private val team1ScoreView = itemView.findViewById<TextView>(R.id.resultsViewTeam1Score)
        private val team2ScoreView = itemView.findViewById<TextView>(R.id.resultsViewTeam2Score)
        private val team1PredictedScoreView =
            itemView.findViewById<TextView>(R.id.resultsViewTeam1PredictedScore)
        private val team2PredictedScoreView =
            itemView.findViewById<TextView>(R.id.resultsViewTeam2PredictedScore)

        fun setMatch(result: MatchResultWithPrediction) {
            team1NameView.text = result.team1
            team2NameView.text = result.team2
            team1ScoreView.text = result.team1EndScore.toString()
            team2ScoreView.text = result.team2EndScore.toString()
            team1PredictedScoreView.text = result.team1PredictedScore?.toString() ?: "_"
            team2PredictedScoreView.text = result.team2PredictedScore?.toString() ?: "_"
        }

    }
}