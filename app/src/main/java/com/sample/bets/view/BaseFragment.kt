package com.sample.bets.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sample.bets.viewmodel.BaseViewModel
import com.sample.bets.viewmodel.Destination
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragment: Fragment() {
    protected abstract val viewModel: BaseViewModel
    protected abstract val layoutId: Int
    private val observables: MutableList<Completable> = mutableListOf()
    private val disposables = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutId, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        disposables.clear()
        observables.clear()
        observeNavigationEvent()
    }

    override fun onResume() {
        observables.forEach { it
            .subscribe()
            .untilPause()
        }
        super.onResume()
    }

    override fun onPause() {
        disposables.clear()
        super.onPause()
    }

    private fun navigate(destination: Destination) = findNavController().run {
        destination
            .args
            ?.let {
                navigate(destination.id, bundleOf(it::class.java.simpleName to it))
                currentBackStackEntry?.savedStateHandle?.set(it::class.java.simpleName, it)
            }
            ?: navigate(destination.id)
    }

    private fun observeNavigationEvent() =
        viewModel
            .observeNavigationEvent()
            .doOnNext(::navigate)
            .ignoreElements()
            .lifecycleAwareSubscription()

    protected fun Completable.lifecycleAwareSubscription() = observables.add(this)

    private fun Disposable.untilPause() = disposables.add(this)
}

typealias DestinationId = Int

