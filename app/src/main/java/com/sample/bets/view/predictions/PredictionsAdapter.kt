package com.sample.bets.view.predictions

import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxrelay2.PublishRelay
import com.sample.bets.R
import com.sample.bets.business.predictions.MatchWithPrediction
import io.reactivex.Observable
import kotlinx.parcelize.Parcelize

class PredictionsAdapter : RecyclerView.Adapter<PredictionsViewHolder>(),
    RecyclerViewItemClickObserver {
    private val internalPredictions = mutableListOf<MatchWithPrediction>()
    private val itemClicks = PublishRelay.create<MatchKey>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PredictionsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.prediction_item, parent, false)
        return PredictionsViewHolder(view)
    }

    override fun onBindViewHolder(holder: PredictionsViewHolder, position: Int) {
        val matchesWithPredictions = internalPredictions[position]
        holder.setMatch(matchesWithPredictions)
        holder.itemView.setOnClickListener {
            itemClicks.accept(
                MatchKey(
                    team1 = matchesWithPredictions.team1,
                    team2 = matchesWithPredictions.team2
                )
            )
        }
    }

    override fun getItemCount(): Int = internalPredictions.size

    override fun observeItemClicks(): Observable<MatchKey> = itemClicks

    fun setPredictions(predictions: List<MatchWithPrediction>) = internalPredictions.apply {
        clear()
        addAll(predictions)
        notifyDataSetChanged()
    }
}

class PredictionsViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {
    private val team1NameView = itemView.findViewById<TextView>(R.id.predictionsViewTeam1)
    private val team2NameView = itemView.findViewById<TextView>(R.id.predictionsViewTeam2)
    private val team1ScoreView = itemView.findViewById<TextView>(R.id.predictionsViewTeam1Score)
    private val team2ScoreView = itemView.findViewById<TextView>(R.id.predictionsViewTeam2Score)

    fun setMatch(match: MatchWithPrediction) {
        team1NameView.text = match.team1
        team2NameView.text = match.team2
        team1ScoreView.text = match.team1Score?.toString() ?: "_"
        team2ScoreView.text = match.team2Score?.toString() ?: "_"
    }

}

interface RecyclerViewItemClickObserver {
    fun observeItemClicks(): Observable<MatchKey>
}

@Parcelize
data class MatchKey(
    val team1: String,
    val team2: String
) : Parcelable