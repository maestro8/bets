package com.sample.bets.view.predictions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jakewharton.rxbinding3.view.clicks
import com.sample.bets.R
import com.sample.bets.business.predictions.Prediction
import com.sample.bets.viewmodel.predictions.EnterPredictionViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

@AndroidEntryPoint
class EnterPredictionView : BottomSheetDialogFragment() {
    private val viewModel: EnterPredictionViewModel by viewModels()
    private val observables: MutableList<Completable> = mutableListOf()
    private val disposables = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        observables.clear()
        disposables.clear()
        val view = inflater.inflate(R.layout.enter_prediction_view, container, false)
        val team1Text = view.findViewById<TextView>(R.id.enterPredictionTeam1Name)
        val team1ScoreView = view.findViewById<TextView>(R.id.enterPredictionTeam1Score)
        val team2ScoreView = view.findViewById<TextView>(R.id.enterPredictionTeam2Score)
        view.findViewById<ImageButton>(R.id.enterPredictionIncreaseTeam1Score)
            .clicks()
            .map {
                val currentValue = team1ScoreView.text
                team1ScoreView.text = (currentValue.toString().toInt() + 1).toString()
            }
            .ignoreElements()
            .lifeCycleAwareSubscription()

        view.findViewById<ImageButton>(R.id.enterPredictionDecreaseTeam1Score)
            .clicks()
            .map {
                val newValue = team1ScoreView.text.toString().toInt() - 1
                if (newValue >= 0) {
                    team1ScoreView.text = newValue.toString()
                }
            }
            .ignoreElements()
            .lifeCycleAwareSubscription()

        view.findViewById<ImageButton>(R.id.enterPredictionIncreaseTeam2Score)
            .clicks()
            .map {
                val currentValue = team2ScoreView.text
                team2ScoreView.text = (currentValue.toString().toInt() + 1).toString()
            }
            .ignoreElements()
            .lifeCycleAwareSubscription()

        view.findViewById<ImageButton>(R.id.enterPredictionDecreaseTeam2Score)
            .clicks()
            .map {
                val newValue = team2ScoreView.text.toString().toInt() - 1
                if (newValue >= 0) {
                    team2ScoreView.text = newValue.toString()
                }
            }
            .ignoreElements()
            .lifeCycleAwareSubscription()

        val team2Text = view.findViewById<TextView>(R.id.enterPredictionTeam2Name)
        val submitButton = view.findViewById<Button>(R.id.submitPrediction)

        submitButton
            .clicks()
            .map {
                viewModel.addPrediction(
                    Prediction(
                        team1 = team1Text.text.toString(),
                        team2 = team2Text.text.toString(),
                        team1Score = team1ScoreView.text.toString().toInt(),
                        team2Score = team2ScoreView.text.toString().toInt()
                    )
                )
            }
            .ignoreElements()
            .lifeCycleAwareSubscription()

        viewModel
            .getPrediction()
            .doOnNext {
                team1Text.text = it.team1
                team2Text.text = it.team2
                team1ScoreView.text = it.team1Score?.toString() ?: "0"
                team2ScoreView.text = it.team2Score?.toString() ?: "0"
            }
            .ignoreElements()
            .lifeCycleAwareSubscription()

        viewModel
            .dismissalStream()
            .doOnNext { dismiss() }
            .ignoreElements()
            .lifeCycleAwareSubscription()

        return view
    }

    override fun onResume() {
        observables.forEach {
            it.subscribe()
                .untilPause()
        }
        super.onResume()
    }

    override fun onPause() {
        disposables.clear()
        super.onPause()
    }

    private fun Completable.lifeCycleAwareSubscription() = observables.add(this)
    private fun Disposable.untilPause() = disposables.add(this)
}