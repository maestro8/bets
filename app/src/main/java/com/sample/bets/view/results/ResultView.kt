package com.sample.bets.view.results

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding3.view.clicks
import com.sample.bets.R
import com.sample.bets.view.BaseFragment
import com.sample.bets.viewmodel.results.MatchResultsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ResultView @Inject constructor() : BaseFragment() {
    override val layoutId: Int = R.layout.results_view
    override val viewModel: MatchResultsViewModel by viewModels()
    private val resultsAdapter: ResultsAdapter = ResultsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Toolbar>(R.id.toolbar)
            .apply {
                inflateMenu(R.menu.menu)
                val item = menu.findItem(R.id.action_results)
                item.isVisible = true
                item.title = resources.getString(R.string.restart)
                item.clicks()
                    .doOnNext { viewModel.deletePredictions() }
                    .ignoreElements()
                    .lifecycleAwareSubscription()
            }

        view.findViewById<RecyclerView>(R.id.results)
            .apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(view.context)
                adapter = resultsAdapter
                addItemDecoration(
                    DividerItemDecoration(
                        this.context,
                        DividerItemDecoration.VERTICAL
                    )
                )
            }

        viewModel
            .observeResults()
            .doOnNext(resultsAdapter::updateData)
            .ignoreElements()
            .lifecycleAwareSubscription()
    }
}