package com.sample.bets.view.predictions

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding3.view.clicks
import com.sample.bets.R
import com.sample.bets.view.BaseFragment
import com.sample.bets.viewmodel.predictions.PredictionsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PredictionsView @Inject constructor() : BaseFragment() {
    override val layoutId: Int = R.layout.predictions_view
    override val viewModel: PredictionsViewModel by viewModels()
    private val predictionsAdapter = PredictionsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Toolbar>(R.id.toolbar)
            .apply {
                inflateMenu(R.menu.menu)
                val item = menu.findItem(R.id.action_results)
                item.isVisible = false
                item.title = resources.getString(R.string.results)
                item.clicks()
                    .doOnNext { viewModel.resultButtonClicked() }
                    .ignoreElements()
                    .lifecycleAwareSubscription()

                viewModel
                    .showResultButton()
                    .map(item::setVisible)
                    .ignoreElements()
                    .lifecycleAwareSubscription()
            }

        viewModel
            .observeMatches()
            .doOnNext(predictionsAdapter::setPredictions)
            .ignoreElements()
            .lifecycleAwareSubscription()

        view.findViewById<RecyclerView>(R.id.predictions).apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(view.context)
            adapter = predictionsAdapter
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        }
        observeItemClicks()
    }

    private fun observeItemClicks() {
        predictionsAdapter
            .observeItemClicks()
            .doOnNext(viewModel::matchSelected)
            .ignoreElements()
            .lifecycleAwareSubscription()
    }
}