package com.sample.bets.business.results.contracts

import com.sample.bets.business.predictions.Match
import com.sample.bets.business.results.MatchResult
import io.reactivex.Maybe
import io.reactivex.Observable

interface MatchesRepository {
    fun getMatch(team1: String, team2: String): Maybe<Match>
    fun getMatches(): Observable<List<Match>>
    fun getMatchesResults(): Observable<List<MatchResult>>
}