package com.sample.bets.business.predictions.contracts

import com.sample.bets.business.predictions.MatchWithPrediction
import io.reactivex.Maybe

interface GetMatchWithPredictionUseCase {
    operator fun invoke(team1: String, team2: String): Maybe<MatchWithPrediction>
}