package com.sample.bets.business.predictions.di

import com.sample.bets.business.predictions.AddPredictionUseCaseImpl
import com.sample.bets.business.predictions.DeleteAllPredictionsUseCaseImpl
import com.sample.bets.business.predictions.GetMatchWithPredictionUseCaseImpl
import com.sample.bets.business.predictions.GetMatchesWithPredictionsUseCaseImpl
import com.sample.bets.business.predictions.contracts.GetMatchWithPredictionUseCase
import com.sample.bets.viewmodel.predictions.contracts.AddPredictionUseCase
import com.sample.bets.viewmodel.predictions.contracts.DeleteAllPredictionsUseCase
import com.sample.bets.viewmodel.predictions.contracts.GetMatchesWithPredictionsUseCase
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class PredictionsModule {
    @Binds
    abstract fun bindGetMatchWithPredictionUseCase(matchWithPrediction: GetMatchWithPredictionUseCaseImpl): GetMatchWithPredictionUseCase

    @Binds
    abstract fun bindGetMatchesResultsWithPredictionsUseCase(matchesWithPredictions: GetMatchesWithPredictionsUseCaseImpl): GetMatchesWithPredictionsUseCase

    @Binds
    abstract fun bindAddPredictionUseCase(addPrediction: AddPredictionUseCaseImpl): AddPredictionUseCase

    @Binds
    abstract fun bindDeleteAllPredictionsUseCase(deleteAllPredictions: DeleteAllPredictionsUseCaseImpl): DeleteAllPredictionsUseCase
}