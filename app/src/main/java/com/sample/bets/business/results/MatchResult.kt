package com.sample.bets.business.results

data class MatchResult(
    val team1: String,
    val team2: String,
    val team1Score: Int,
    val team2Score: Int,
)