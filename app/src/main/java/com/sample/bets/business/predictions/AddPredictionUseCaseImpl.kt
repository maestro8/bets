package com.sample.bets.business.predictions

import com.sample.bets.business.predictions.contracts.PredictionsRepository
import com.sample.bets.viewmodel.predictions.contracts.AddPredictionUseCase
import javax.inject.Inject

class AddPredictionUseCaseImpl @Inject constructor(
    private val predictionsRepositoryImpl: PredictionsRepository
) : AddPredictionUseCase {
    override fun invoke(prediction: Prediction) =
        predictionsRepositoryImpl.addPrediction(prediction)
}
