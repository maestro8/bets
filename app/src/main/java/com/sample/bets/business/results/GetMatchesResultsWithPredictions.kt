package com.sample.bets.business.results

import com.sample.bets.business.predictions.contracts.PredictionsRepository
import com.sample.bets.business.results.contracts.MatchesRepository
import com.sample.bets.viewmodel.results.contracts.GetMatchesResultsWithPredictionsUseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetMatchesResultsWithPredictionsUseCaseImpl @Inject constructor(
    private val matchesRepositoryImpl: MatchesRepository,
    private val predictionsRepositoryImpl: PredictionsRepository

) : GetMatchesResultsWithPredictionsUseCase {
    override fun invoke(): Observable<List<MatchResultWithPrediction>> =
        Observable.combineLatest(
            matchesRepositoryImpl.getMatchesResults(),
            predictionsRepositoryImpl.getAllPredictions()
        ) { results, predictions ->
            results.map { match ->
                val prediction = predictions.find { prediction ->
                    match.team1 == prediction.team1 && match.team2 == prediction.team2
                }
                MatchResultWithPrediction(
                    team1 = match.team1,
                    team2 = match.team2,
                    team1EndScore = match.team1Score,
                    team2EndScore = match.team2Score,
                    team1PredictedScore = prediction?.team1Score,
                    team2PredictedScore = prediction?.team2Score
                )
            }
        }
            .distinctUntilChanged()
}

data class MatchResultWithPrediction(
    val team1: String,
    val team2: String,
    val team1EndScore: Int,
    val team2EndScore: Int,
    val team1PredictedScore: Int?,
    val team2PredictedScore: Int?
)