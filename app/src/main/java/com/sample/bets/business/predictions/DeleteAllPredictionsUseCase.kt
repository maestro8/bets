package com.sample.bets.business.predictions

import com.sample.bets.business.predictions.contracts.PredictionsRepository
import com.sample.bets.viewmodel.predictions.contracts.DeleteAllPredictionsUseCase
import javax.inject.Inject

class DeleteAllPredictionsUseCaseImpl @Inject constructor(
    private val predictionsRepositoryImpl: PredictionsRepository
): DeleteAllPredictionsUseCase {
    override fun invoke() = predictionsRepositoryImpl.deleteAllPredictions()
}