package com.sample.bets.business.predictions

import com.sample.bets.business.predictions.contracts.GetMatchWithPredictionUseCase
import com.sample.bets.business.predictions.contracts.PredictionsRepository
import com.sample.bets.business.results.contracts.MatchesRepository
import io.reactivex.Maybe
import javax.inject.Inject

class GetMatchWithPredictionUseCaseImpl @Inject constructor(
    private val predictionsRepository: PredictionsRepository,
    private val matchesRepositoryImpl: MatchesRepository
) : GetMatchWithPredictionUseCase {
    override fun invoke(team1: String, team2: String): Maybe<MatchWithPrediction> =
        predictionsRepository.getPrediction(team1 = team1, team2 = team2)
            .map {
                MatchWithPrediction(
                    team1 = it.team1,
                    team2 = it.team2,
                    team1Score = it.team1Score,
                    team2Score = it.team2Score
                )
            }
            .switchIfEmpty(
                matchesRepositoryImpl.getMatch(
                    team1 = team1,
                    team2 = team2
                ).map {
                    MatchWithPrediction(
                        team1 = it.team1,
                        team2 = it.team2,
                        team1Score = null,
                        team2Score = null
                    )
                }
            )
}