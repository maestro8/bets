package com.sample.bets.business.predictions

import com.sample.bets.business.predictions.contracts.PredictionsRepository
import com.sample.bets.business.results.contracts.MatchesRepository
import com.sample.bets.viewmodel.predictions.contracts.GetMatchesWithPredictionsUseCase
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import javax.inject.Inject

class GetMatchesWithPredictionsUseCaseImpl @Inject constructor(
    private val matchesRepositoryImpl: MatchesRepository,
    private val predictionsRepositoryImpl: PredictionsRepository
) : GetMatchesWithPredictionsUseCase {
    override fun invoke(): Observable<List<MatchWithPrediction>> =
        Observables.combineLatest(
            matchesRepositoryImpl
                .getMatches(),
            predictionsRepositoryImpl.getAllPredictions()
        ) { matches, predictions ->
            matches.map { matchData ->
                val predictionsData = predictions.firstOrNull {
                    it.team1 == matchData.team1 && it.team2 == matchData.team2
                }
                MatchWithPrediction(
                    team1 = matchData.team1,
                    team2 = matchData.team2,
                    team1Score = predictionsData?.team1Score,
                    team2Score = predictionsData?.team2Score,
                )
            }
        }
}
