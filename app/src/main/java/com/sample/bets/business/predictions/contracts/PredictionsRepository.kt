package com.sample.bets.business.predictions.contracts

import com.sample.bets.business.predictions.Prediction
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

interface PredictionsRepository {
    fun addPrediction(prediction: Prediction): Completable
    fun getPrediction(team1: String, team2: String): Maybe<Prediction>
    fun getAllPredictions(): Observable<List<Prediction>>
    fun deleteAllPredictions(): Completable
}