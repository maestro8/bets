package com.sample.bets.business.predictions

data class Match(
    val team1: String,
    val team2: String
)