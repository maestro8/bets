package com.sample.bets.business.results.di

import com.sample.bets.viewmodel.results.contracts.GetMatchesResultsWithPredictionsUseCase
import com.sample.bets.business.results.GetMatchesResultsWithPredictionsUseCaseImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class ResultsModule {
    @Binds
    abstract fun bindGetMatchesResultsWithPredictions(resultsWithPredictions: GetMatchesResultsWithPredictionsUseCaseImpl): GetMatchesResultsWithPredictionsUseCase
}