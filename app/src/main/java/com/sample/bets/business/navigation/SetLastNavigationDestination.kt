package com.sample.bets.business

import com.sample.bets.repository.NavigationRepository
import com.sample.bets.viewmodel.Destination
import com.sample.bets.viewmodel.navigation.contracts.SetLastNavigationDestination
import javax.inject.Inject

class SetLastNavigationDestinationImpl @Inject constructor(
    private val navigationRepository: NavigationRepository
) : SetLastNavigationDestination {
    override fun invoke(destination: Destination) =
        navigationRepository.setTheLatestNavigationDestination(destination.id)
}
