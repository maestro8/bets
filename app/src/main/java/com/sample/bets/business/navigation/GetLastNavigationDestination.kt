package com.sample.bets.business

import com.sample.bets.repository.NavigationRepository
import com.sample.bets.viewmodel.Destination
import com.sample.bets.viewmodel.navigation.contracts.GetLastNavigationDestination
import javax.inject.Inject

class GetLastNavigationDestinationImpl @Inject constructor(
    private val navigationRepository: NavigationRepository
) : GetLastNavigationDestination {
    override fun invoke(): Destination = object : Destination {
        override val id: Int = navigationRepository.getTheLatestNavigationDestination()
    }
}

