package com.sample.bets.business.di

import com.sample.bets.business.GetLastNavigationDestinationImpl
import com.sample.bets.business.SetLastNavigationDestinationImpl
import com.sample.bets.viewmodel.navigation.contracts.GetLastNavigationDestination
import com.sample.bets.viewmodel.navigation.contracts.SetLastNavigationDestination
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class BusinessModule {
    @Binds
    abstract fun bindSetLastNavigationDestination(setDestination: SetLastNavigationDestinationImpl): SetLastNavigationDestination

    @Binds
    abstract fun bindGetLastNavigationDestination(getDestination: GetLastNavigationDestinationImpl): GetLastNavigationDestination
}