package com.sample.bets.viewmodel.predictions.contracts

import io.reactivex.Completable

interface DeleteAllPredictionsUseCase {
    operator fun invoke(): Completable
}