package com.sample.bets.viewmodel.navigation.contracts

import com.sample.bets.viewmodel.Destination

interface GetLastNavigationDestination {
    operator fun invoke(): Destination
}