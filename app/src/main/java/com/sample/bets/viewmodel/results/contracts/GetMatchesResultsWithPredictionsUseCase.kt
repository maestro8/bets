package com.sample.bets.viewmodel.results.contracts

import com.sample.bets.business.results.MatchResultWithPrediction
import io.reactivex.Observable

interface GetMatchesResultsWithPredictionsUseCase {
    operator fun invoke(): Observable<List<MatchResultWithPrediction>>
}