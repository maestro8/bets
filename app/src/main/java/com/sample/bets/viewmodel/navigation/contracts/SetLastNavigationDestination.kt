package com.sample.bets.viewmodel.navigation.contracts

import com.sample.bets.viewmodel.Destination

interface SetLastNavigationDestination {
    operator fun invoke(destination: Destination)
}