package com.sample.bets.viewmodel.results

import com.jakewharton.rxrelay2.BehaviorRelay
import com.sample.bets.business.results.MatchResultWithPrediction
import com.sample.bets.viewmodel.BaseViewModel
import com.sample.bets.viewmodel.navigation.PredictionsDestination
import com.sample.bets.viewmodel.predictions.contracts.DeleteAllPredictionsUseCase
import com.sample.bets.viewmodel.results.contracts.GetMatchesResultsWithPredictionsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class MatchResultsViewModel @Inject constructor(
    getMatchesResultsWithPredictions: GetMatchesResultsWithPredictionsUseCase,
    private val deleteAllPredictionsUseCase: DeleteAllPredictionsUseCase
) : BaseViewModel() {
    private val resultsStream: BehaviorRelay<List<MatchResultWithPrediction>> =
        BehaviorRelay.create()

    init {
        getMatchesResultsWithPredictions()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(resultsStream::accept)
            .untilViewModelCleared()
    }

    fun observeResults(): Observable<List<MatchResultWithPrediction>> = resultsStream

    fun deletePredictions() = deleteAllPredictionsUseCase()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { navigateTo(PredictionsDestination) }
        .untilViewModelCleared()
}