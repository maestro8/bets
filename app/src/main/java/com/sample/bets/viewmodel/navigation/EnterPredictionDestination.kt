package com.sample.bets.viewmodel.navigation

import android.os.Parcelable
import com.sample.bets.NavGraph
import com.sample.bets.view.predictions.MatchKey
import com.sample.bets.viewmodel.Destination

class EnterPredictionDestination(matchKey: MatchKey) : Destination {
    override val id: Int = NavGraph.Destination.enterPrediction
    override val args: Parcelable = matchKey
}