package com.sample.bets.viewmodel.predictions.contracts

import com.sample.bets.business.predictions.MatchWithPrediction
import io.reactivex.Observable

interface GetMatchesWithPredictionsUseCase {
    operator fun invoke(): Observable<List<MatchWithPrediction>>
}