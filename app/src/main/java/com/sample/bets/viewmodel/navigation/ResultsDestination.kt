package com.sample.bets.viewmodel.navigation

import com.sample.bets.NavGraph
import com.sample.bets.viewmodel.Destination

object ResultsDestination : Destination {
    override val id: Int = NavGraph.Destination.results
}