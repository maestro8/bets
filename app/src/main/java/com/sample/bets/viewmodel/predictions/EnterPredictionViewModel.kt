package com.sample.bets.viewmodel.predictions

import androidx.lifecycle.SavedStateHandle
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.sample.bets.business.predictions.MatchWithPrediction
import com.sample.bets.business.predictions.Prediction
import com.sample.bets.business.predictions.contracts.GetMatchWithPredictionUseCase
import com.sample.bets.view.predictions.MatchKey
import com.sample.bets.viewmodel.BaseViewModel
import com.sample.bets.viewmodel.predictions.contracts.AddPredictionUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class EnterPredictionViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    getMatchWithPredictionUseCase: GetMatchWithPredictionUseCase,
    private val addPredictionUseCase: AddPredictionUseCase
) : BaseViewModel() {
    private val matchWithPredictionRelay: BehaviorRelay<MatchWithPrediction> =
        BehaviorRelay.create()
    private val dismissalStream: PublishRelay<Unit> = PublishRelay.create()

    init {
        val matchKey: MatchKey? = savedStateHandle[MatchKey::class.java.simpleName]
        if (matchKey == null) dismissalStream.accept(Unit)
        else {
            getMatchWithPredictionUseCase(team1 = matchKey.team1, team2 = matchKey.team2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(matchWithPredictionRelay::accept)
                .untilViewModelCleared()
        }
    }

    fun dismissalStream() = dismissalStream
    fun getPrediction(): Observable<MatchWithPrediction> = matchWithPredictionRelay
    fun addPrediction(prediction: Prediction) = addPredictionUseCase(prediction)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { dismissalStream.accept(Unit) }
        .untilViewModelCleared()
}