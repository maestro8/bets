package com.sample.bets.viewmodel.predictions.contracts

import com.sample.bets.business.predictions.Prediction
import io.reactivex.Completable

interface AddPredictionUseCase {
    operator fun invoke(prediction: Prediction): Completable
}