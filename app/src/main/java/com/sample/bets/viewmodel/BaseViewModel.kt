package com.sample.bets.viewmodel

import android.os.Parcelable
import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface Destination {
    val id: Int
    val args: Parcelable?
        get() = null
}

abstract class BaseViewModel: ViewModel(){
    private val navigationEventStream: PublishRelay<Destination> = PublishRelay.create()
    private val disposables: CompositeDisposable = CompositeDisposable()
    fun observeNavigationEvent(): Observable<Destination> = navigationEventStream
    protected fun navigateTo(destination: Destination) = navigationEventStream.accept(destination)
    protected fun Disposable.untilViewModelCleared() = disposables.add(this)

    override fun onCleared() {
        disposables.dispose()
    }
}