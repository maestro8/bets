package com.sample.bets.viewmodel.navigation

import com.sample.bets.viewmodel.navigation.contracts.GetLastNavigationDestination
import com.sample.bets.viewmodel.navigation.contracts.SetLastNavigationDestination
import com.sample.bets.view.DestinationId
import com.sample.bets.viewmodel.BaseViewModel
import com.sample.bets.viewmodel.Destination
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NavigationViewModel @Inject constructor(
    private val getTheLastNavigationDestination: GetLastNavigationDestination,
    private val setLastNavigationDestination: SetLastNavigationDestination
) : BaseViewModel() {
    fun setNavigationDestination(id: DestinationId) = setLastNavigationDestination(
        object : Destination {
            override val id: Int = id
        }
    )

    fun getNavigationDestination(): Destination = getTheLastNavigationDestination()
}