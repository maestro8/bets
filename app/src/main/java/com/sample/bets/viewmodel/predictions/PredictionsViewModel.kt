package com.sample.bets.viewmodel.predictions

import com.jakewharton.rxrelay2.BehaviorRelay
import com.sample.bets.business.predictions.MatchWithPrediction
import com.sample.bets.view.predictions.MatchKey
import com.sample.bets.viewmodel.BaseViewModel
import com.sample.bets.viewmodel.navigation.EnterPredictionDestination
import com.sample.bets.viewmodel.navigation.ResultsDestination
import com.sample.bets.viewmodel.predictions.contracts.GetMatchesWithPredictionsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class PredictionsViewModel @Inject constructor(
    getMatchesWithPredictionsUseCase: GetMatchesWithPredictionsUseCase,
) : BaseViewModel() {
    private val matchesStream: BehaviorRelay<List<MatchWithPrediction>> =
        BehaviorRelay.create()

    init {
        getMatchesWithPredictionsUseCase
            .invoke()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(matchesStream::accept)
            .untilViewModelCleared()
    }

    fun matchSelected(match: MatchKey) = navigateTo(EnterPredictionDestination(match))
    fun observeMatches(): Observable<List<MatchWithPrediction>> = matchesStream
    fun resultButtonClicked() = navigateTo(ResultsDestination)
    fun showResultButton(): Observable<Boolean> = matchesStream
        .map { matches ->
            matches.firstOrNull { it.team1Score != null && it.team2Score != null }
                ?.let { true }
                ?: false
        }
}