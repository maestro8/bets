package com.sample.bets

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BetsApplication: Application()