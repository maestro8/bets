package com.sample.bets.repository.matches

import com.sample.bets.business.predictions.Match
import com.sample.bets.business.results.MatchResult
import com.sample.bets.business.results.contracts.MatchesRepository
import com.sample.bets.repository.CachingTimer
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MatchesRepositoryImpl @Inject constructor(
    private val matchesService: MatchesService,
    private val matchDao: MatchDao,
    private val matchResultDao: MatchResultDao,
    private val cachingTimer: CachingTimer,
    private val matchResultSanityChecker: MatchResultSanityChecker,
    private val matchSanityChecker: MatchSanityChecker
) : MatchesRepository {
    override fun getMatches(): Observable<List<Match>> = matchDao
        .getAll()
        .publish { databaseStream ->
            Observable.merge(
                databaseStream
                    .firstElement()
                    .defaultIfEmpty(emptyList())
                    .flatMapObservable {
                        if (it.isEmpty()) {
                            retrieveUpcomingMatches()
                        } else {
                            cachingTimer(it.first().insertedAt)
                                .flatMap { retrieveUpcomingMatches() }
                        }
                    },
                databaseStream
            )
        }
        .map { it.map(MatchData::toMatch) }

    override fun getMatch(team1: String, team2: String): Maybe<Match> = matchDao
        .get(team1 = team1, team2 = team2)
        .map(MatchData::toMatch)

    override fun getMatchesResults(): Observable<List<MatchResult>> = matchResultDao
        .getAll()
        .publish { databaseStream ->
            Observable.merge(
                databaseStream
                    .firstElement()
                    .defaultIfEmpty(emptyList())
                    .flatMapObservable {
                        if (it.isEmpty()) {
                            retrieveMatchesResults()
                        } else {
                            cachingTimer(it.first().insertedAt)
                                .flatMap { retrieveMatchesResults() }
                        }
                    },
                databaseStream
            )
        }
        .map { it.map(MatchResultData::toMatchResult) }

    private fun retrieveUpcomingMatches() =
        matchesService
            .getUpcomingMatches()
            .map { it.toMatchData(matchSanityChecker) }
            .flatMapCompletable {
                matchDao
                    .deleteAll()
                    .andThen(matchDao.insertAll(it))
            }
            .toObservable<List<MatchData>>()

    private fun retrieveMatchesResults() = matchesService
        .getMatchesResults()
        .subscribeOn(Schedulers.io())
        .map { it.toMatchResultData(matchResultSanityChecker) }
        .flatMapCompletable {
            matchResultDao
                .deleteAll()
                .andThen(matchResultDao.insertAll(it))
        }
        .toObservable<List<MatchResultData>>()
}

private fun MatchData.toMatch() = Match(
    team1 = team1,
    team2 = team2
)

private fun MatchResultData.toMatchResult() = MatchResult(
    team1 = team1,
    team2 = team2,
    team1Score = team1Score,
    team2Score = team2Score
)