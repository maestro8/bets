package com.sample.bets.repository.matches

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable

@Entity(primaryKeys = ["team1", "team2"])
class MatchResultData(
    val team1: String,
    val team2: String,
    val team1Score: Int,
    val team2Score: Int,
    val insertedAt: Long
)

@Dao
interface MatchResultDao {
    @Query("SELECT * FROM MatchResultData")
    fun getAll(): Observable<List<MatchResultData>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(matchesResults: List<MatchResultData>): Completable

    @Delete
    fun delete(matchResult: MatchResultData)

    @Query("DELETE FROM MatchResultData")
    fun deleteAll(): Completable
}