package com.sample.bets.repository.predictions

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

@Entity(primaryKeys = ["team1", "team2"])
data class PredictionData(
    val team1: String,
    val team2: String,
    val team1Score: Int,
    val team2Score: Int
)

@Dao
interface PredictionDao {
    @Query("SELECT * FROM PredictionData")
    fun getAll(): Observable<List<PredictionData>>

    @Query("SELECT * FROM PredictionData WHERE team1 = :team1 AND team2 = :team2")
    fun getPrediction(team1: String, team2: String): Maybe<PredictionData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(predictionsData: PredictionData): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(predictionsData: List<PredictionData>): Completable

    @Update
    fun update(predictionsData: PredictionData)

    @Delete
    fun delete(predictionsData: PredictionData)

    @Query("DELETE FROM PredictionData")
    fun deleteAll(): Completable
}