package com.sample.bets.repository

import android.content.SharedPreferences
import javax.inject.Inject

const val SHARED_PREFERENCES_LATEST_NAVIGATION_DESTINATION =
    "shared_preferences_latest_navigation_destination"

interface NavigationRepository {
    fun getTheLatestNavigationDestination(): Int
    fun setTheLatestNavigationDestination(destinationId: Int)
}

class NavigationRepositoryImpl @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : NavigationRepository {
    override fun getTheLatestNavigationDestination() =
        sharedPreferences.getInt(SHARED_PREFERENCES_LATEST_NAVIGATION_DESTINATION, -1)

    override fun setTheLatestNavigationDestination(destinationId: Int) =
        sharedPreferences
            .edit()
            .putInt(SHARED_PREFERENCES_LATEST_NAVIGATION_DESTINATION, destinationId)
            .apply()
}