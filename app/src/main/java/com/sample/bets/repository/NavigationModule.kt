package com.sample.bets.repository

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NavigationModule {
    @Binds
    abstract fun bindNavigationRepository(navigationRepository: NavigationRepositoryImpl): NavigationRepository
}