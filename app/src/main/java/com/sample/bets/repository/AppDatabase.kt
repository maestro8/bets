package com.sample.bets.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sample.bets.repository.matches.MatchDao
import com.sample.bets.repository.matches.MatchData
import com.sample.bets.repository.matches.MatchResultDao
import com.sample.bets.repository.matches.MatchResultData
import com.sample.bets.repository.predictions.PredictionDao
import com.sample.bets.repository.predictions.PredictionData

@Database(
    entities = [
        MatchData::class,
        PredictionData::class,
        MatchResultData::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun matchDao(): MatchDao
    abstract fun predictionsDao(): PredictionDao
    abstract fun matchesResultsDao(): MatchResultDao
}