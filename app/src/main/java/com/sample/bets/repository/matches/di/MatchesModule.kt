package com.sample.bets.repository.matches

import com.sample.bets.business.results.contracts.MatchesRepository
import com.sample.bets.repository.AppDatabase
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class MatchesModule {
    @Binds
    abstract fun bindMatchesRepository(matchesRepositoryImpl: MatchesRepositoryImpl): MatchesRepository
}

@Module
@InstallIn(SingletonComponent::class)
object MatchesDataSourceModule{
    @Provides
    fun provideMatchesService(retrofit: Retrofit): MatchesService = retrofit.create(MatchesService::class.java)

    @Provides
    @Singleton
    fun provideMatcherDao(database: AppDatabase) = database.matchDao()

    @Provides
    @Singleton
    fun provideMatchesResultsDao(database: AppDatabase) = database.matchesResultsDao()
}