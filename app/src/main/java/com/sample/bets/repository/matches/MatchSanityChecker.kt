package com.sample.bets.repository.matches

import javax.inject.Inject

class MatchSanityChecker @Inject constructor() {
    operator fun invoke(match: MatchResponse) = match.run {
        team1.isNotBlank() && team2.isNotBlank()
    }
}