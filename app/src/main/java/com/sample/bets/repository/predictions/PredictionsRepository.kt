package com.sample.bets.repository.predictions

import com.sample.bets.business.predictions.contracts.PredictionsRepository
import com.sample.bets.business.predictions.Prediction
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class PredictionsRepositoryImpl @Inject constructor(
    private val predictionsDao: PredictionDao
) : PredictionsRepository {
    override fun addPrediction(prediction: Prediction) = predictionsDao
        .insert(prediction.toPredictionData())

    override fun getAllPredictions(): Observable<List<Prediction>> = predictionsDao
        .getAll()
        .map { it.map(PredictionData::toPrediction) }

    override fun getPrediction(team1: String, team2: String) = predictionsDao
        .getPrediction(
            team1 = team1,
            team2 = team2
        ).map(PredictionData::toPrediction)

    override fun deleteAllPredictions(): Completable = predictionsDao.deleteAll()
}

fun Prediction.toPredictionData() = PredictionData(
    team1 = team1,
    team2 = team2,
    team1Score = team1Score,
    team2Score = team2Score
)

fun PredictionData.toPrediction(): Prediction = Prediction(
    team1 = team1,
    team2 = team2,
    team1Score = team1Score,
    team2Score = team2Score
)



