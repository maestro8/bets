package com.sample.bets.repository.predictions.di

import com.sample.bets.repository.AppDatabase
import com.sample.bets.business.predictions.contracts.PredictionsRepository
import com.sample.bets.repository.predictions.PredictionsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class PredictionsModule {
    @Binds
    abstract fun bindPredictionsRepository(predictionsRepository: PredictionsRepositoryImpl): PredictionsRepository
}

@Module
@InstallIn(SingletonComponent::class)
object PredictionsDataSourceModule {
    @Provides
    @Singleton
    fun providePredictionsDao(database: AppDatabase) = database.predictionsDao()
}