package com.sample.bets.repository.matches

import io.reactivex.Single
import retrofit2.http.GET

interface MatchesService {
    @GET("b428bff2-0bb7-4ab0-b43c-641de261d285")
    fun getUpcomingMatches(): Single<MatchesResponse>

    @GET("f749507b-0123-4bb8-8007-a7a7d796f7a3")
    fun getMatchesResults(): Single<MatchesResultsResponse>
}

