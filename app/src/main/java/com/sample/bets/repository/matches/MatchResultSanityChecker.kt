package com.sample.bets.repository.matches

import javax.inject.Inject

class MatchResultSanityChecker @Inject constructor() {
    operator fun invoke(matchResponse: MatchResultResponse) = matchResponse.run {
        team1.isNotBlank()
                && team2.isNotBlank()
                && team1Score >= 0
                && team2Score >= 0
    }
}