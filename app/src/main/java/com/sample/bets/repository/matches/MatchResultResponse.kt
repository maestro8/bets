package com.sample.bets.repository.matches

import com.squareup.moshi.Json

data class MatchesResultsResponse(
    val matches: List<MatchResultResponse>
)

data class MatchResultResponse(
    val team1: String,
    val team2: String,
    @field:Json(name = "team1_points")
    val team1Score: Int,
    @field:Json(name = "team2_points")
    val team2Score: Int
)

fun MatchesResultsResponse.toMatchResultData(sanityChecker: MatchResultSanityChecker) =
    matches
        .filter(sanityChecker::invoke)
        .map {
        MatchResultData(
            team1 = it.team1,
            team2 = it.team2,
            team1Score = it.team1Score,
            team2Score = it.team2Score,
            insertedAt = System.currentTimeMillis()
        )
    }