package com.sample.bets.repository.matches

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

@Entity(primaryKeys = ["team1", "team2"])
data class MatchData(
    val team1: String,
    val team2: String,
    val insertedAt: Long
)

@Dao
interface MatchDao {
    @Query("SELECT * FROM MatchData")
    fun getAll(): Observable<List<MatchData>>

    @Query("SELECT * FROM MatchData WHERE team1 = :team1 AND team2 = :team2")
    fun get(team1: String, team2: String): Maybe<MatchData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(matchData: List<MatchData>): Completable

    @Delete
    fun delete(matchData: MatchData)

    @Query("DELETE FROM MatchResultData")
    fun deleteAll(): Completable
}