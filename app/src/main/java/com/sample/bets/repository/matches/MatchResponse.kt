package com.sample.bets.repository.matches

data class MatchesResponse(
    val matches: List<MatchResponse>
)

data class MatchResponse(
    val team1: String,
    val team2: String
)

fun MatchesResponse.toMatchData(sanityChecker: MatchSanityChecker) =
    matches
        .filter(sanityChecker::invoke)
        .map {
        MatchData(
            team1 = it.team1,
            team2 = it.team2,
            insertedAt = System.currentTimeMillis()
        )
    }