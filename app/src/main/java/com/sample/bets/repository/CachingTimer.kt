package com.sample.bets.repository

import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CachingTimer @Inject constructor() {
    operator fun invoke(lastUpdate: Long): Observable<Unit> {
        val diffBetweenLastUpdatedTime = System.currentTimeMillis() - lastUpdate
        return if (diffBetweenLastUpdatedTime < 60000) {
            Observable.timer(60000 - diffBetweenLastUpdatedTime, TimeUnit.MILLISECONDS)
                .map { Unit }
        } else Observable.just(Unit)
    }
}