package com.sample.bets

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.createGraph
import androidx.navigation.fragment.NavHostFragment
import com.sample.bets.viewmodel.navigation.NavigationViewModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.EntryPointAccessors

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val navigationViewModel: NavigationViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host) as NavHostFragment

        val destinationProvidersEntryPoints = EntryPointAccessors.fromActivity(
            this,
            DestinationProvidersEntryPoints::class.java
        )

        navHostFragment
            .navController
            .apply {
                graph = createGraph(NavGraph.id, NavGraph.Destination.predictions) {
                    destinationProvidersEntryPoints().forEach { it.invoke(this) }
                }
                val savedDestination = navigationViewModel.getNavigationDestination().id
                if (savedDestination != -1 && currentDestination?.id != savedDestination) {
                    navigate(savedDestination)
                }
                addOnDestinationChangedListener { _, destination, _ ->
                    if (destination.id != NavGraph.Destination.enterPrediction) {
                        navigationViewModel.setNavigationDestination(destination.id)
                    }
                }
            }
    }
}


